class DocSettings:
    header = "<html>"
    footer = "</html>"
    external = {}
    hide_undocumented = True
    show_member_protection_in_list = True
    show_member_type_in_list = True
    show_source_files = True
    show_file_paths = True
    keep_empty_member_sections = False
    args = None
